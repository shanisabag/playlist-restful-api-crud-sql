export interface IArtist {
    first_name: string;
    last_name: string;
}

export interface ISong {
    name: string;
    artist_id: number;
}

export interface IPlaylist {
    name: string;
    user_id: number;
}

export interface IUser {
    first_name: string;
    last_name: string;
    email: string;
    password: string;
    refresh_token: string;
    role_id: number;
}

export interface Iobj {
    [key: string]: string;
}

declare global {
    // eslint-disable-next-line @typescript-eslint/no-namespace
    namespace Express {
        interface Request {
            id: string;
            user_id: number;
            role: number;
        }
    }
}
