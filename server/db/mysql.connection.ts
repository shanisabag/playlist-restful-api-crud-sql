import log from "@ajar/marker";
import mysql from "mysql2/promise";

export let connection: mysql.Connection;

export const connect = async () => {
    if (connection) return connection;
    connection = await mysql.createConnection({
        host: process.env.DB_HOST,
        port: Number(process.env.DB_PORT),
        database: process.env.DB_NAME,
        user: process.env.DB_USER_NAME,
        password: process.env.DB_USER_PASSWORD,
    });
    await connection.connect();
    log.magenta(" ✨ Connected to MySql DB ✨ ");
};
