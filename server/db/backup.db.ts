import fetch from "node-fetch";
import { exec } from "child_process";
import databases from "../../config.json";

const obj_db = JSON.parse(JSON.stringify(databases));

export const backup_server = async (url: string) => {
    for (const key in obj_db) {
        try {
            const task = exec(
                `docker exec mysql-db /usr/bin/mysqldump -u root --password=qwerty ${key}`
            );
            
            const file_name = `${Date.now()}${obj_db[key]}`;

            const result = await fetch(`http://${url}`, {
                method: "POST",
                body: task.stdout,
                headers: { file_name },
            });

            const data = await result.json();
            console.log(data);
        } catch (error) {
            console.log(error);
            console.log("could not backup server");
        }
    }
};
