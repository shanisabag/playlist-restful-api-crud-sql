import cron from "node-cron";

import { backup_server } from "../db/backup.db.js";

export const backup_scheduler = (url: string) => {
    cron.schedule("*/30 * * * * *", async () => {
        await backup_server(url as string);
    });
};
