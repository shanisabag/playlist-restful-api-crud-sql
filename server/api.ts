import express from "express";
import morgan from "morgan";
import log from "@ajar/marker";
import cors from "cors";

import { connect as connect_db } from "./db/mysql.connection.js";
import { backup_scheduler } from "./scheduler/backup.scheduler.js";
import artist_router from "./modules/artist/artist.router.js";
import song_router from "./modules/song/song.router.js";
import playlist_router from "./modules/playlist/playlist.router.js";
import auth_router from "./modules/auth/auth.router.js";
import user_router from "./modules/user/user.router.js";
import admin_router from "./modules/admin/admin.router.js";
import {
    loggerError,
    responseWithError,
    urlNotFound,
} from "./middleware/errors.handler.js";
import { setID } from "./middleware/id.middleware.js";
import { logger } from "./middleware/logger.middleware.js";
class start_api {
    private _PORT: number;
    private _HOST: string;
    private _app;
    private _LOG_FILE_PATH = "./server/logs/users.log";
    private _ERROR_LOG_FILE_PATH = "./server/logs/errors.log";
    private _storage_url = process.env.STORAGE_SERVER;

    constructor() {
        this._HOST = process.env.HOST || "localhost";
        this._PORT = Number(process.env.PORT);
        this._app = express();
    }

    init() {
        // middleware
        this._app.use(cors());
        this._app.use(morgan("dev"));
        this._app.use(express.json());
        this._app.use(setID);
        this._app.use(logger(this._LOG_FILE_PATH)); // log each http request to users.log

        // routing
        this.routing();

        // error handling
        this.errors_handler();

        //start the express api server
        this.connect_to_db().catch(console.log);
    }

    private routing() {
        this._app.use("/api/auth", auth_router);
        this._app.use("/api/users", user_router);
        this._app.use("/api/artists", artist_router);
        this._app.use("/api/songs", song_router);
        this._app.use("/api/playlists", playlist_router);
        this._app.use("/api/admin", admin_router);
    }

    private errors_handler() {
        this._app.use("*", urlNotFound);
        this._app.use(loggerError(this._ERROR_LOG_FILE_PATH));
        this._app.use(responseWithError);
    }

    private async connect_to_db() {
        //connect to mongo db
        await connect_db();
        this._app.listen(this._PORT, this._HOST);
        log.magenta(
            "api is live on",
            ` ✨ ⚡  http://${this._HOST}:${this._PORT} ✨ ⚡`
        );
        backup_scheduler(this._storage_url as string);
    }
}

const app = new start_api();
app.init();
