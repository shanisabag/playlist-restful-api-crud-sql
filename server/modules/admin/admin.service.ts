import { ResultSetHeader, RowDataPacket } from "mysql2";

import { connection } from "../../db/mysql.connection.js";
import HttpException from "../../exceptions/http.exception.js";
import * as artist_service from "../artist/artist.service.js";
import * as song_service from "../song/song.service.js";

export const get_artists_requests = async () => {
    const sql_select = "SELECT * FROM artist WHERE status_id = 3";
    const [pendding_artists] = (await connection.query(
        sql_select
    )) as RowDataPacket[];
    return pendding_artists;
};

export const get_songs_requests = async () => {
    const sql_select = "SELECT * FROM song WHERE status_id = 3";
    const [pendding_songs] = (await connection.query(
        sql_select
    )) as RowDataPacket[];
    return pendding_songs;
};

export const update_artist_status_by_id = async (artist_id: number) => {
    const artist = await artist_service.get_artist_by_id(artist_id);
    if (artist.length === 0) {
        throw new HttpException(404, "artist is not exists.");
    }

    const { first_name, last_name, status_id } = artist[0];
    // if sataus_id = 1 - the request is already accepted!
    if (status_id === 1) {
        throw new HttpException(202, "artist is already accepted");
    }

    const is_artist_exists = await artist_service.is_artist_exists(
        first_name,
        last_name
    );
    // 2 - reject , 1 - accept
    const status = is_artist_exists ? 2 : 1;

    // update status
    const sql_update = `UPDATE artist SET ? WHERE artist_id = ${artist_id}`;
    const [result] = (await connection.query(sql_update, {
        status_id: status,
    })) as ResultSetHeader[];

    const updated_artist = await artist_service.get_artist_by_id(artist_id);
    return updated_artist;
};

export const update_song_status_by_id = async (song_id: number) => {
    const song = await song_service.get_song_by_id(song_id);
    if (song.length === 0) {
        throw new HttpException(404, "song is not exists.");
    }
    const { name, artist_id, status_id } = song[0];
    // if sataus_id = 1 - the request is already accepted!
    if (status_id === 1) {
        throw new HttpException(202, "song is already accepted");
    }

    const is_song_exists = await song_service.is_song_exists(name, artist_id);
    // 2 - reject , 1 - accept
    const status = is_song_exists ? 2 : 1;

    // update status
    const sql_update = `UPDATE song SET ? WHERE song_id = ${song_id}`;
    const [result] = (await connection.query(sql_update, {
        status_id: status,
    })) as ResultSetHeader[];

    const updated_song = await song_service.get_song_by_id(song_id);
    return updated_song;
};
