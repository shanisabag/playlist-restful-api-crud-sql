import { Request, Response } from "express";

import UrlNotFoundException from "../../exceptions/urlNotFound.exception.js";
import * as admin_service from "./admin.service.js";

export const get_artists_requests = async (req: Request, res: Response) => {
    const pendding_artists = await admin_service.get_artists_requests();
    res.status(200).json(pendding_artists);
};

export const get_songs_requests = async (req: Request, res: Response) => {
    const pendding_songs = await admin_service.get_songs_requests();
    res.status(200).json(pendding_songs);
};

export const update_artist_status_by_id = async (
    req: Request,
    res: Response
) => {
    const updated_artist = await admin_service.update_artist_status_by_id(
        Number(req.params.artist_id)
    );

    if (updated_artist.length === 0)
        throw new UrlNotFoundException(`/api/admin${req.path}`);
    res.status(200).json(updated_artist);
};

export const update_song_status_by_id = async (req: Request, res: Response) => {
    const updated_song = await admin_service.update_song_status_by_id(
        Number(req.params.song_id)
    );

    if (updated_song.length === 0)
        throw new UrlNotFoundException(`/api/admin${req.path}`);
    res.status(200).json(updated_song);
};
