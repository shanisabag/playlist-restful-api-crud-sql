import express from "express";

import raw from "../../middleware/route.async.wrapper.js";
import { verify_admin } from "../../middleware/verify.admin.js";
import { verify_auth } from "../../middleware/verify.auth.js";
import {
    get_artists_requests,
    get_songs_requests,
    update_artist_status_by_id,
    update_song_status_by_id,
} from "./admin.controller.js";

const router = express.Router();

router.use(verify_auth);
router.use(verify_admin); // verify admin middaleware

// get artists' requests
router.get("/requests/artists", raw(get_artists_requests));

// get songs' requests
router.get("/requests/songs", raw(get_songs_requests));

// update artist's status
router.patch("/requests/artist/:artist_id", raw(update_artist_status_by_id));

// update song's status
router.patch("/requests/song/:song_id", raw(update_song_status_by_id));

export default router;
