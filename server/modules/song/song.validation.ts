import { Iobj, ISong } from "../../typing.js";
import { add_song_schema, update_song_schema } from "./song.scheme.js";

export const add_song_validation = async (obj: ISong) => {
    await add_song_schema.validateAsync(obj);
};

export const update_song_validation = async (obj: Iobj) => {
    await update_song_schema.validateAsync(obj);
};
