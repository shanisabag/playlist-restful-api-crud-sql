import { ResultSetHeader, RowDataPacket } from "mysql2";

import { connection } from "../../db/mysql.connection.js";
import { Iobj, ISong } from "../../typing.js";

export const get_song_by_id = async (song_id: number) => {
    const sql_select = `SELECT * FROM song WHERE song.song_id = ${song_id}`;
    const [song] = (await connection.query(sql_select)) as RowDataPacket[];
    return song;
};

export const get_all_songs = async () => {
    const sql_select = "SELECT * FROM song";
    const [songs] = (await connection.query(sql_select)) as RowDataPacket[];
    return songs;
};

export const create_song = async (payload: ISong) => {
    const sql_insert = "INSERT INTO song SET ?";
    const [result] = await connection.query(sql_insert, payload);

    // return created song
    const song: any = result;
    const sql_select = `SELECT * FROM song WHERE song_id = ${song.insertId}`;
    const [created_song] = (await connection.query(
        sql_select
    )) as RowDataPacket[];

    return created_song[0];
};

export const get_artist_songs = async (artist_id: number) => {
    const sql_select = `SELECT * FROM song WHERE artist_id = ${artist_id}`;
    const [songs] = (await connection.query(sql_select)) as RowDataPacket[];
    return songs;
};

export const update_song_by_id = async (song_id: number, payload: Iobj) => {
    const sql_update = `UPDATE song SET ? WHERE song_id = ${song_id}`;
    const [result] = (await connection.query(
        sql_update,
        payload
    )) as ResultSetHeader[];

    const updated_song = await get_song_by_id(song_id);
    return updated_song;
};

export const delete_song_by_id = async (song_id: number) => {
    const song = await get_song_by_id(song_id);

    const sql_delete = `DELETE FROM song WHERE song_id = ${song_id}`;
    const [result] = (await connection.query(sql_delete)) as ResultSetHeader[];

    return song;
};

// check if song exists - same name and artist_id
export const is_song_exists = async (name: string, artist_id: number) => {
    const sql_select = `SELECT * FROM song WHERE status_id = 1 AND song.name = "${name}" AND song.artist_id = ${artist_id}`;
    const [same_song] = (await connection.query(sql_select)) as RowDataPacket[];

    return same_song.length === 0 ? false : true;
};
