import { Request, Response } from "express";

import UrlNotFoundException from "../../exceptions/urlNotFound.exception.js";
import * as song_service from "./song.service.js";
import * as artist_service from "../artist/artist.service.js";
import {
    add_song_validation,
    update_song_validation,
} from "./song.validation.js";

export const get_all_songs = async (req: Request, res: Response) => {
    const songs = await song_service.get_all_songs();
    res.status(200).json(songs);
};

export const create_song = async (req: Request, res: Response) => {
    await add_song_validation(req.body);
    const song = await song_service.create_song(req.body);
    res.status(200).json(song);
};

export const get_artist_songs = async (req: Request, res: Response) => {
    const artist_id = Number(req.params.artist_id);
    // check if artist exists
    const artist = await artist_service.get_artist_by_id(artist_id);
    if (artist.length === 0)
        throw new UrlNotFoundException(`/api/songs${req.path}`);

    const songs = await song_service.get_artist_songs(artist_id);
    res.status(200).json(songs);
};

export const get_song_by_id = async (req: Request, res: Response) => {
    const song = await song_service.get_song_by_id(Number(req.params.song_id));
    // check if song exists
    if (song.length === 0)
        throw new UrlNotFoundException(`/api/songs${req.path}`);
    res.status(200).json(song);
};

export const update_song_by_id = async (req: Request, res: Response) => {
    await update_song_validation(req.body);
    const song = await song_service.update_song_by_id(
        Number(req.params.song_id),
        req.body
    );
    // check if song exists
    if (song.length === 0)
        throw new UrlNotFoundException(`/api/songs${req.path}`);
    res.status(200).json(song);
};

export const delete_song_by_id = async (req: Request, res: Response) => {
    const song = await song_service.delete_song_by_id(
        Number(req.params.song_id)
    );
    // check if song exists
    if (song.length === 0)
        throw new UrlNotFoundException(`/api/songs${req.path}`);
    res.status(200).json(song);
};
