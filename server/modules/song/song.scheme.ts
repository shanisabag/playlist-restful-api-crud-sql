import Joi from "joi";

export const add_song_schema = Joi.object({
    name: Joi.string().min(2).max(20).required(),
    artist_id: Joi.number().required(),
});

export const update_song_schema = Joi.object({
    name: Joi.string().min(2).max(20),
});
