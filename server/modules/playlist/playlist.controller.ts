import { Request, Response } from "express";

import UrlNotFoundException from "../../exceptions/urlNotFound.exception.js";
import * as playlist_service from "./playlist.service.js";
import * as song_service from "../song/song.service.js";
import {
    add_playlist_validation,
    update_playlist_validation,
} from "./playlist.validation.js";
import HttpException from "../../exceptions/http.exception.js";

export const get_all_playlists = async (req: Request, res: Response) => {
    const playlists = await playlist_service.get_all_playlists();
    res.status(200).json(playlists);
};

export const create_playlist = async (req: Request, res: Response) => {
    await add_playlist_validation(req.body);
    const playlist = await playlist_service.create_playlist(
        req.body,
        req.user_id
    );
    res.status(200).json(playlist);
};

export const add_song_to_playlist = async (req: Request, res: Response) => {
    const { playlist_id, song_id } = req.params;

    // check if song's status is accepted
    const song = await song_service.get_song_by_id(Number(song_id));
    const { status_id } = song[0];
    if (status_id !== 1)
        throw new HttpException(
            400,
            "this song is not accepted yet. try to add different song."
        );

    // check if playlist.user_id === req.user_id || admin
    const has_permission = await playlist_service.has_permission_to_update(
        Number(playlist_id),
        req.user_id,
        req.role
    );
    if (!has_permission) {
        throw new HttpException(
            403,
            "you do not have permission to add song to this playlist."
        );
    }

    const playlist_has_song = await playlist_service.add_song_to_playlist(
        Number(playlist_id),
        Number(song_id)
    );

    res.status(200).json(playlist_has_song);
};

export const remove_song_from_playlist = async (
    req: Request,
    res: Response
) => {
    const { playlist_id, song_id } = req.params;

    // check if playlist.user_id === req.user_id || admin
    const has_permission = await playlist_service.has_permission_to_update(
        Number(playlist_id),
        req.user_id,
        req.role
    );
    if (!has_permission) {
        throw new HttpException(
            403,
            "you do not have permission to remove song from this playlist."
        );
    }

    const playlist = await playlist_service.remove_song_from_playlist(
        Number(playlist_id),
        Number(song_id)
    );
    res.status(200).json(playlist);
};

export const get_playlist_songs = async (req: Request, res: Response) => {
    const playlist = await playlist_service.get_playlist_songs(
        Number(req.params.playlist_id)
    );
    res.status(200).json(playlist);
};

export const update_playlist_by_id = async (req: Request, res: Response) => {
    const playlist_id = Number(req.params.playlist_id);

    await update_playlist_validation(req.body);

    // check if playlist.user_id === req.user_id || admin
    const has_permission = await playlist_service.has_permission_to_update(
        playlist_id,
        req.user_id,
        req.role
    );
    if (!has_permission) {
        throw new HttpException(
            403,
            "you do not have permission to update this playlist."
        );
    }

    const playlist = await playlist_service.update_playlist_by_id(
        playlist_id,
        req.body
    );
    // check if playlist exists
    if (playlist.length === 0)
        throw new UrlNotFoundException(`/api/playlists${req.path}`);
    res.status(200).json(playlist);
};

export const delete_playlist_by_id = async (req: Request, res: Response) => {
    const playlist_id = Number(req.params.playlist_id);
    // check if playlist.user_id === req.user_id || admin
    const has_permission = await playlist_service.has_permission_to_update(
        playlist_id,
        req.user_id,
        req.role
    );
    if (!has_permission) {
        throw new HttpException(
            403,
            "you do not have permission to update this playlist."
        );
    }

    const playlist = await playlist_service.delete_playlist_by_id(playlist_id);
    // check if playlist exists
    if (playlist.length === 0)
        throw new UrlNotFoundException(`/api/playlists${req.path}`);
    res.status(200).json(playlist);
};
