import { ResultSetHeader, RowDataPacket } from "mysql2";

import { connection } from "../../db/mysql.connection.js";
import { Iobj, IPlaylist } from "../../typing.js";

export const get_playlist_by_id = async (playlist_id: number) => {
    const sql_select = `SELECT * FROM playlist WHERE playlist_id = ${playlist_id}`;
    const [playlist] = (await connection.query(sql_select)) as RowDataPacket[];
    return playlist;
};

// get playlist's songs
export const get_playlist_songs = async (playlist_id: number) => {
    const sql_select = `SELECT song.* FROM playlist_has_song LEFT JOIN song ON playlist_has_song.song_id = song.song_id WHERE playlist_has_song.playlist_id = ${playlist_id}`;
    const [playlist_songs] = (await connection.query(
        sql_select
    )) as RowDataPacket[];

    return playlist_songs;
};

export const get_all_playlists = async () => {
    const sql_select = "SELECT * FROM playlist";
    const [playlists] = (await connection.query(sql_select)) as RowDataPacket[];
    return playlists;
};

export const create_playlist = async (payload: IPlaylist, user_id: number) => {
    const sql_insert = "INSERT INTO playlist SET ?";
    const [result] = await connection.query(sql_insert, {
        ...payload,
        user_id,
    });

    // return created playlist
    const playlist: any = result;
    const sql_select = `SELECT * FROM playlist WHERE playlist_id = ${playlist.insertId}`;
    const [created_playlist] = (await connection.query(
        sql_select
    )) as RowDataPacket[];

    return created_playlist[0];
};

export const add_song_to_playlist = async (
    playlist_id: number,
    song_id: number
) => {
    const sql_insert = "INSERT INTO playlist_has_song SET ?";
    const [result] = await connection.query(sql_insert, {
        playlist_id,
        song_id,
    });

    // return playlists's songs
    const playlist = await get_playlist_songs(playlist_id);
    return playlist;
};

export const remove_song_from_playlist = async (
    playlist_id: number,
    song_id: number
) => {
    const sql_delete = `DELETE FROM playlist_has_song WHERE playlist_id = ${playlist_id} AND song_id = ${song_id}`;
    const [result] = (await connection.query(sql_delete)) as ResultSetHeader[];

    // return playlists's songs
    const playlist = await get_playlist_songs(playlist_id);
    return playlist;
};

export const update_playlist_by_id = async (
    playlist_id: number,
    payload: Iobj
) => {
    const sql_update = `UPDATE playlist SET ? WHERE playlist_id = ${playlist_id}`;
    const [updated_playlist] = (await connection.query(
        sql_update,
        payload
    )) as ResultSetHeader[];

    const playlist = await get_playlist_by_id(playlist_id);
    return playlist;
};

export const delete_playlist_by_id = async (playlist_id: number) => {
    const playlist = await get_playlist_by_id(playlist_id);

    const sql_delete = `DELETE FROM playlist WHERE playlist_id = ${playlist_id}`;
    const [result] = (await connection.query(sql_delete)) as ResultSetHeader[];

    return playlist;
};

export const has_permission_to_update = async (
    playlist_id: number,
    req_user_id: number,
    req_role: number
) => {
    const playlist = await get_playlist_by_id(playlist_id);
    const { user_id } = playlist[0];
    if (user_id === req_user_id || req_role === 1) return true;
    return false;
};
