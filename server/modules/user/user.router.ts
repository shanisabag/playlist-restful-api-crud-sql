import express from "express";

import raw from "../../middleware/route.async.wrapper.js";
import { verify_admin } from "../../middleware/verify.admin.js";
import { verify_auth } from "../../middleware/verify.auth.js";
import {
    delete_user,
    get_all_users,
    get_user_data,
    update_user,
} from "./user.controller.js";

const router = express.Router();

// get user's data
router.get("/", verify_auth, raw(get_user_data));

// get all users - only admin
router.get("/all", verify_auth, verify_admin, raw(get_all_users));

// update a user
router.patch("/:userID", verify_auth, raw(update_user));

// delete a user
router.delete("/:userID", verify_auth, verify_admin, raw(delete_user));

export default router;
