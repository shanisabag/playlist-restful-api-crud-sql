import { IUser } from "../../typing.js";
import { add_user_schema, update_user_schema } from "./user.schema.js";

export const add_user_validation = async (obj: IUser) => {
    await add_user_schema.validateAsync(obj);
};

export const update_user_validation = async (obj: IUser) => {
    await update_user_schema.validateAsync(obj);
};
