import { ResultSetHeader, RowDataPacket } from "mysql2";

import { connection } from "../../db/mysql.connection.js";
import { Iobj } from "../../typing.js";

export const get_user_by_id = async (user_id: number) => {
    const sql_select = `SELECT * FROM user WHERE user_id = ${user_id}`;
    const [user] = (await connection.query(sql_select)) as RowDataPacket[];
    return user[0];
};

export const get_user_data = async (user_id: number) => {
    const sql_select = `SELECT user.first_name, user.last_name, user.email, GROUP_CONCAT(distinct playlist.name) as playlists FROM user LEFT JOIN playlist ON user.user_id = playlist.user_id WHERE user.user_id = ${user_id}`;
    const [user] = (await connection.query(sql_select)) as RowDataPacket[];
    return user[0];
};

export const get_all_users = async () => {
    const sql_select = "SELECT * FROM user";
    const [users] = (await connection.query(sql_select)) as RowDataPacket[];
    console.log(users, "users");
    return users;
};

export const update_user = async (user_id: number, payload: Iobj) => {
    const sql_update = `UPDATE user SET ? WHERE user_id = ${user_id}`;
    const [result] = (await connection.query(
        sql_update,
        payload
    )) as ResultSetHeader[];
    const updated_user = await get_user_data(user_id);
    return updated_user;
};

export const delete_user = async (user_id: number) => {
    const user = await get_user_by_id(user_id);
    const sql_delete = `DELETE FROM user WHERE user_id = ${user}`;
    const [result] = (await connection.query(sql_delete)) as ResultSetHeader[];
    return user;
};
