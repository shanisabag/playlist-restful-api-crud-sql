import Joi from "joi";

export const add_user_schema = Joi.object({
    first_name: Joi.string().min(2).max(20).required(),
    last_name: Joi.string().min(2).max(20).required(),
    email: Joi.string()
        .email({ minDomainSegments: 2, tlds: { allow: ["com", "net"] } })
        .required(),
    password: Joi.string().min(6).required(),
    role: Joi.number().min(1).max(2),
});

export const update_user_schema = Joi.object({
    first_name: Joi.string().min(2).max(10),
    last_name: Joi.string().min(2).max(10),
});
