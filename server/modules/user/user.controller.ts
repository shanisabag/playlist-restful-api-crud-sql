import { Request, Response } from "express";

import UrlNotFoundException from "../../exceptions/urlNotFound.exception.js";
import * as user_service from "./user.service.js";
import { update_user_validation } from "./user.validation.js";

export const get_user_data = async (req: Request, res: Response) => {
    const user = await user_service.get_user_data(req.user_id);
    res.status(200).json(user);
};

export const get_all_users = async (req: Request, res: Response) => {
    const users = await user_service.get_all_users();
    res.status(200).json(users);
};

export const update_user = async (req: Request, res: Response) => {
    await update_user_validation(req.body);
    const user = await user_service.update_user(req.user_id, req.body);
    if (!user) throw new UrlNotFoundException(`/api/users${req.path}`);
    res.status(200).json(user);
};

export const delete_user = async (req: Request, res: Response) => {
    const user = await user_service.delete_user(req.user_id);
    if (!user) throw new UrlNotFoundException(`/api/users${req.path}`);
    res.status(200).json(user);
};
