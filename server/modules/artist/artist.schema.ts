import Joi from "joi";

export const add_artist_schema = Joi.object({
    first_name: Joi.string().min(2).max(10).required(),
    last_name: Joi.string().min(2).max(10).required(),
});

export const update_artist_schema = Joi.object({
    first_name: Joi.string().min(2).max(10),
    last_name: Joi.string().min(2).max(10),
});
