import { ResultSetHeader, RowDataPacket } from "mysql2";

import { IArtist, Iobj } from "../../typing.js";
import { connection } from "../../db/mysql.connection.js";

export const get_artist_by_id = async (artist_id: number) => {
    const sql_select = `SELECT * FROM artist WHERE artist.artist_id = ${artist_id}`;
    const [artist] = (await connection.query(sql_select)) as RowDataPacket[];
    return artist;
};

export const get_all_artists = async () => {
    const sql_select = "SELECT * FROM artist";
    const [artists] = (await connection.query(sql_select)) as RowDataPacket[];
    return artists;
};

export const create_artist = async (payload: IArtist) => {
    const sql_insert = "INSERT INTO artist SET ?";
    const [result] = await connection.query(sql_insert, payload);

    // return created artist
    const artist: any = result;
    const sql_select = `SELECT * FROM artist WHERE artist_id = ${artist.insertId}`;
    const [created_artist] = (await connection.query(
        sql_select
    )) as RowDataPacket[];

    return created_artist[0];
};

export const update_artist_by_id = async (artist_id: number, payload: Iobj) => {
    const sql_update = `UPDATE artist SET ? WHERE artist_id = ${artist_id}`;
    const [result] = (await connection.query(
        sql_update,
        payload
    )) as ResultSetHeader[];

    const updated_artist = await get_artist_by_id(artist_id);
    return updated_artist;
};

export const delete_artist_by_id = async (artist_id: number) => {
    const artist = await get_artist_by_id(artist_id);

    const sql_delete = `DELETE FROM artist WHERE artist_id = ${artist_id}`;
    const [result] = (await connection.query(sql_delete)) as ResultSetHeader[];

    return artist;
};

// check if artist exists - same first and last name
export const is_artist_exists = async (
    first_name: string,
    last_name: string
) => {
    const sql_select = `SELECT * FROM artist WHERE status_id = 1 AND artist.first_name = "${first_name}" AND artist.last_name = "${last_name}"`;
    const [same_artist] = (await connection.query(
        sql_select
    )) as RowDataPacket[];

    return same_artist.length === 0 ? false : true;
};
