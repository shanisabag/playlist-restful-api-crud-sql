import express from "express";

import raw from "../../middleware/route.async.wrapper.js";
import { verify_admin } from "../../middleware/verify.admin.js";
import { verify_auth } from "../../middleware/verify.auth.js";
import {
    create_artist,
    delete_artist_by_id,
    get_all_artists,
    get_artist_by_id,
    update_artist_by_id,
} from "./artist.controller.js";

const router = express.Router();

// get all artists
router.get("/", raw(get_all_artists));

// create a new artist
router.post("/", verify_auth, raw(create_artist));

// get an artist by id
router.get("/:artist_id", raw(get_artist_by_id));

// update an artist by id
router.patch(
    "/:artist_id",
    verify_auth,
    verify_admin,
    raw(update_artist_by_id)
);

// delete an artist by id
router.delete(
    "/:artist_id",
    verify_auth,
    verify_admin,
    raw(delete_artist_by_id)
);

export default router;
