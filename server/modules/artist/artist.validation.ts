import { IArtist, Iobj } from "../../typing.js";
import { add_artist_schema, update_artist_schema } from "./artist.schema.js";

export const add_artist_validation = async (obj: IArtist) => {
    await add_artist_schema.validateAsync(obj);
};

export const update_artist_validation = async (obj: Iobj) => {
    await update_artist_schema.validateAsync(obj);
};
