import { Request, Response } from "express";

import UrlNotFoundException from "../../exceptions/urlNotFound.exception.js";
import * as artist_service from "./artist.service.js";
import {
    add_artist_validation,
    update_artist_validation,
} from "./artist.validation.js";

export const get_all_artists = async (req: Request, res: Response) => {
    const artists = await artist_service.get_all_artists();
    res.status(200).json(artists);
};

export const create_artist = async (req: Request, res: Response) => {
    await add_artist_validation(req.body);
    const artist = await artist_service.create_artist(req.body);
    res.status(200).json(artist);
};

export const get_artist_by_id = async (req: Request, res: Response) => {
    const artist = await artist_service.get_artist_by_id(
        Number(req.params.artist_id)
    );
    // check if artist exists
    if (artist.length === 0)
        throw new UrlNotFoundException(`/api/artists${req.path}`);
    res.status(200).json(artist);
};

export const update_artist_by_id = async (req: Request, res: Response) => {
    await update_artist_validation(req.body);
    const artist = await artist_service.update_artist_by_id(
        Number(req.params.artist_id),
        req.body
    );
    // check if artist exists
    if (artist.length === 0)
        throw new UrlNotFoundException(`/api/artists${req.path}`);

    res.status(200).json(artist);
};

export const delete_artist_by_id = async (req: Request, res: Response) => {
    const artist = await artist_service.delete_artist_by_id(
        Number(req.params.artist_id)
    );
    // check if artist exists
    if (artist.length === 0)
        throw new UrlNotFoundException(`/api/artists${req.path}`);

    res.status(200).json(artist);
};
