import jwt from "jsonwebtoken";
import ms from "ms";
import bcrypt from "bcrypt";
import { ResultSetHeader, RowDataPacket } from "mysql2";
import { Request, Response } from "express";

import HttpException from "../../exceptions/http.exception.js";
import { connection } from "../../db/mysql.connection.js";
import { add_user_validation } from "../user/user.validation.js";

const { APP_SECRET, ACCESS_TOKEN_EXPIRATION, REFRESH_TOKEN_EXPIRATION } =
    process.env;

export const get_access_token = async (req: Request, res: Response) => {
    //get refresh_token from client - req.cookies
    const { refresh_token } = req.cookies;

    if (!refresh_token)
        throw new HttpException(
            403,
            "No refresh_token provided. Please login."
        );

    try {
        // verifies secret and checks expiration
        const decoded = await jwt.verify(refresh_token, APP_SECRET as string);

        //check user refresh token in DB
        const { id, role, profile } = decoded as jwt.JwtPayload;

        const access_token = jwt.sign(
            { id, role, some: "other value" },
            APP_SECRET as string,
            {
                expiresIn: ACCESS_TOKEN_EXPIRATION, //expires in 1 minute
            }
        );

        req.user_id = id;
        req.role = role;

        res.status(200).json(
            `
                access token: ${access_token},
                role: ${role},
                profile: ${profile}
            `
        );
    } catch (error) {
        throw new HttpException(
            401,
            "Unauthorized - Failed to verify refresh_token."
        );
    }
};

export const register = async (req: Request, res: Response) => {
    await add_user_validation(req.body);

    const { first_name, last_name, email, password, role } = req.body;

    // hash password
    const hash_pwd = await bcrypt.hash(password, 7);

    // add user to db
    const sql_insert = `INSERT INTO user (first_name, last_name, email, password, role_id) VALUES ("${first_name}", "${last_name}", "${email}", "${hash_pwd}", ${role})`;
    const [result] = await connection.query(sql_insert);

    // return created user
    const user: any = result;
    const sql_select = `SELECT first_name, last_name, email FROM user WHERE user_id = ${user.insertId}`;
    const [created_user] = (await connection.query(
        sql_select
    )) as RowDataPacket[];
    res.status(200).json(created_user[0]);
};

export const login = async (req: Request, res: Response) => {
    const { email, password } = req.body;
    const sql_select = `SELECT * FROM user WHERE email = "${email}"`;
    const [user] = (await connection.query(sql_select)) as RowDataPacket[];
    if (!user[0]) throw new HttpException(403, "wrong email or password");

    const same_pwd = await bcrypt.compare(password, user[0].password);
    if (same_pwd) {
        const user_id = user[0].user_id;
        const user_role = user[0].role_id;

        // create an access_token
        const access_token = jwt.sign(
            { id: user_id, role: user_role, some: "other value" },
            APP_SECRET as string,
            {
                expiresIn: ACCESS_TOKEN_EXPIRATION, // expires in 1 minute
            }
        );

        // create a refresh_token
        const refresh_token = jwt.sign(
            { id: user_id, role: user_role, profile: JSON.stringify(user[0]) },
            APP_SECRET as string,
            {
                expiresIn: REFRESH_TOKEN_EXPIRATION, // expires in 60 days... long-term...
            }
        );

        // save refresh per user in DB
        const sql_update = `UPDATE user SET refresh_token = "${refresh_token}" WHERE user_id = ${user_id}`;
        const [updated_user] = (await connection.query(
            sql_update
        )) as ResultSetHeader[];

        res.cookie("refresh_token", refresh_token, {
            maxAge: ms("60d"), //60 days
            httpOnly: true,
        });

        req.user_id = user_id;
        req.role = user_role;

        // response with access token ****
        res.status(200).json(`access token: ${access_token}`);
    } else {
        throw new HttpException(403, "wrong email or password");
    }
};

export const logout = async (req: Request, res: Response) => {
    // clear refresh_token from user in DB
    const user_id = req.user_id;

    const sql_update = `UPDATE user SET refresh_token = NULL WHERE user_id = ${user_id}`;
    const [updated_user] = (await connection.query(
        sql_update
    )) as ResultSetHeader[];

    res.clearCookie("refresh_token");
    res.status(200).json({ status: "You are logged out" });
};
