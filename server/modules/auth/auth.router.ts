import express from "express";
import cookieParser from "cookie-parser";

import raw from "../../middleware/route.async.wrapper.js";
import {
    get_access_token,
    login,
    logout,
    register,
} from "./auth.controller.js";
import { verify_auth } from "../../middleware/verify.auth.js";

const router = express.Router();
router.use(cookieParser());

router.get("/get-access-token", get_access_token);

// register
router.post("/register", raw(register));

// login
router.post("/login", raw(login));

// logout
router.get("/logout", verify_auth, raw(logout));

export default router;
