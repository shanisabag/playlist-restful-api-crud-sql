import { NextFunction, Request, Response } from "express";
import jwt from "jsonwebtoken";

import HttpException from "../exceptions/http.exception.js";
import raw from "./route.async.wrapper.js";

const { APP_SECRET } = process.env;

export const verify_auth = raw(
    async (req: Request, res: Response, next: NextFunction) => {
        const access_token = req.headers["x-access-token"];
        if (!access_token) throw new HttpException(403, "No token provided.");

        // verifies secret and checks exp
        const decoded = await jwt.verify(
            access_token as string,
            APP_SECRET as string
        );

        // if everything is good, save to request for use in other routes
        req.user_id = (decoded as jwt.JwtPayload).id;
        req.role = (decoded as jwt.JwtPayload).role;
        next();
    }
);
