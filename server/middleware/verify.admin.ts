import { NextFunction, Request, Response } from "express";

import HttpException from "../exceptions/http.exception.js";
import raw from "./route.async.wrapper.js";

export const verify_admin = raw(
    async (req: Request, res: Response, next: NextFunction) => {
        if (req.role !== 1) {
            throw new HttpException(403, "you do not have permission.");
        }
        next();
    }
);
