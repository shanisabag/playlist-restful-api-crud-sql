import { NextFunction, Request, Response } from "express";
import v4 from "uuid";
const { v4: uuidv4 } = v4;

export const setID = async (
    req: Request,
    res: Response,
    next: NextFunction
) => {
    req.id = uuidv4();
    next();
};
